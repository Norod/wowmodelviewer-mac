
---------------------------------
	  macOS - Build notes
---------------------------------

This is how I was able to build WoW-Model-Viewer for macOS Catalina.
This is not a nice way, it's very patchy with a lots of steps.

---------------------------------
			Homebrew
---------------------------------

Make sure you have Xcode's command line tools:
xcode-select --install

If you don't already have Homebrew:
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

Homebrew packages needed for WMV:
brew install cmake autoconf glew llvm libjpeg libpng iconv libxml2 lxml
brew install wxwidgets --with-static --with-stl 

---------------------------------
	Ugly patches for wxwidgets
---------------------------------

In the area of /usr/local/Cellar/wxmac/3.0.4_2/include/wx-3.0/wx

Open the file 'defs.h' and comment out the following:

//DECLARE_WXCOCOA_OBJC_CLASS(NSString);
//DECLARE_WXCOCOA_OBJC_CLASS(NSObject);

Later, when the cringe has weakened and you will be trying to compile, you will get errors. 
For each error, replace WX_NSObject with void*


---------------------------------
			QT
---------------------------------

I Downloaded the official QT installer for macOS from:
https://www.qt.io/offline-installers

When I did that, it installed v5.12.2 so be sure to update the settings based on the version you have installed.
I installed QT to /Users/Shared/Qt

---------------------------------
		 ~/.bash_profile
---------------------------------

export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL=
export LDFLAGS="-L/Users/Shared/Qt/5.12.2/clang_64/lib $LDFLAGS"
export CPPFLAGS="-I/Users/Shared/Qt/5.12.2/clang_64/include  $CPPFLAGS"
export PATH="/usr/local/opt/llvm/bin:/usr/local/sbin:/usr/local/bin:/Users/Shared/Qt/5.12.2/clang_64/bin:$PATH"
export QTDIR="/Users/Shared/Qt/"

---------------------------------
		FBX SDK
---------------------------------

Download the macOS FBX SDK from:
https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-2019-0

Assuming version 2019.2 and default installation location,
Header files should now be at: "/Applications/Autodesk/FBX SDK/2019.2/include/"
Library files should now be at: "/Applications/Autodesk/FBX SDK/2019.2/lib/clang/"

------------------------------------------
	CMAKE - Generating Xcode projects
------------------------------------------

Assuming you have cloned the repo to ~/Projects/GitHub/wowmodelviewer

cd ~/Projects/GitHub/wowmodelviewer/src
mkdir build
cd build

source ~/.bash_profile 
export WMV_BASE_PATH=~/Projects/GitHub/wowmodelviewer
CMAKE_C_COMPILER=$(xcrun -find cc)
CMAKE_CXX_COMPILER=$(xcrun -find c++)
cmake -G 'Xcode' -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER -DCMAKE_CXX_COMPILER=$CMAKE_CXX_COMPILER -DCMAKE_BUILD_TYPE=Debug ../

------------------------------------------
	Manual editing of Xcode project
------------------------------------------

If all went well you should have generated a project file at ~/Projects/GitHub/wowmodelviewer/src/build/WoWModelViewer.xcodeproj
Let's edit it, for example, using bbedit's "Search & Replace all" tool:

bbedit ~/Projects/GitHub/wowmodelviewer/src/build/WoWModelViewer.xcodeproj/project.pbxproj 
 
Search:
-lcasc
Replace with:
-framework casc 

Search: FRAMEWORK_SEARCH_PATHS
Replace content to contain the full path of your QT and casc libs, for example:
Replace:
FRAMEWORK_SEARCH_PATHS = (/Users/Shared/Qt/5.12.2/clang_64/lib);
With:
FRAMEWORK_SEARCH_PATHS = ( 					/Users/Shared/Qt/5.12.2/clang_64/lib, 					/Users/dadler/Projects/GitHub/wowmodelviewer/src/build/casclib/lib, 				);


Search:
-llibfbxsdk.lib
Debug - Replace with:
\"/Applications/Autodesk/FBX SDK/2019.2/lib/clang/debug/libfbxsdk.a\"
Release - Replace with:
\"/Applications/Autodesk/FBX SDK/2019.2/lib/clang/release/libfbxsdk.a\"


------------------------------------------
					Xcode
------------------------------------------

open ~/Projects/GitHub/wowmodelviewer/src/build/WoWModelViewer.xcodeproj

Try building the "ALL_BUILD" aggregated target.
It will fail.

"Fix" wx related errors by replacing WX_NSObject with void*

in 
FileStream.cpp
add
#include <stdio.h>

in
CascCommon.cpp 
add
#include <wchar.h>

in CascPort.h
add
#include <pthread.h>
above
typedef pthread_mutex_t CASC_LOCK;

Fix FBX header related errors by adding "/Applications/Autodesk/FBX SDK/2019.2/include/" to the relevant search path
Fix FBX linker related errors by adding "/Applications/Autodesk/FBX SDK/2019.2/lib/clang/" to the relevant search

Add the following linker flags where needed:
-lxml2
-liconv

Add the string you added to FRAMEWORK_SEARCH_PATHS above, for example: 
/Users/Shared/Qt/5.12.2/clang_64/lib
to wowmodelviewer target's "Run Search Paths"

------------------------------------------
  Once you managed to build everything
------------------------------------------

cd ~/Projects/GitHub/wowmodelviewer/src/build/wowmodelviewer/Debug
mkdir userSettings
mkdir games
cp -r ../../../../bin_support/ ./games/
mkdir plugins
cp ../../plugins/exporters/obj/Debug/libobjexporter.dylib ./plugins/
cp ../../plugins/exporters/fbx/Debug/libfbxexporter.dylib ./plugins/
cp ../../plugins/importers/armory/Debug/libarmory.dylib ./plugins/
cp ../../plugins/importers/wowhead/Debug/libwowhead.dylib ./plugins/

------------------------------------------
  	    Running it for the first time
------------------------------------------

From another terminal tab/window run:
tail -f ./userSettings/log.txt 
to see your run logs

You can now run: 
./wowmodelviewer  
from command line but for this run, choose "No" when asked to load you "WoW" data
Exit WoW-Model-Viewer from its window (File -> Quit)

A file named Config.ini should now be available at ./userSettings

cat ./userSettings/Config.ini 


You can now run from Command line and Run with Debugger using Xcode
Below is an example Config.ini

------------------------------------------
  	    Example Config.ini
------------------------------------------

[Graphics]
AccumulationBuffer=0
AlphaBits=0
ColourBits=24
DoubleBuffer=1
FSAA=0
Fov=45
HWAcceleration=0
SampleBuffer=0
StencilBuffer=0
UseEnvMapping=true
ZBuffer=16

[Locale]
LanguageID=1
LanguageName=enUS

[Session]
BackgroundImage=
CanvasHeight=589
CanvasWidth=823
DBackground=false
InitPoseOnlyExport=false
Layout="layout2|name=canvas;caption=OpenGL Canvas;state=768;dir=5;layer=0;row=0;pos=0;prop=100000;bestw=20;besth=20;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=fileControl;caption=File List;state=2099196;dir=4;layer=0;row=1;pos=0;prop=100000;bestw=170;besth=700;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=2;floaty=23;floatw=170;floath=716|name=animControl;caption=Animation;state=2099196;dir=3;layer=1;row=0;pos=0;prop=100000;bestw=700;besth=120;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=charControl;caption=Character;state=2099198;dir=2;layer=2;row=0;pos=0;prop=100000;bestw=170;besth=700;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=Lighting;caption=Lighting;state=2098943;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=160;besth=430;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=170;floath=430|name=Models;caption=Models;state=2099151;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=182;besth=1097;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=160;floath=460|name=ModelBank;caption=ModelBank;state=2098943;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=270;besth=280;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=300;floath=320|name=Settings;caption=Settings;state=2098883;dir=4;layer=0;row=0;pos=0;prop=100000;bestw=405;besth=550;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=400;floath=550|dock_size(5,0,0)=22|dock_size(3,1,0)=139|dock_size(4,0,1)=253|"
Model=character/thinhuman/male/thinhumanmale
PositionX=0
PositionY=-900
RandomLooks=true
ShowParticle=true
ZeroParticle=true
bgCol=#475F79

[Settings]
ArmoryPath=
CustomDirPath=
CustomFilesConflictPolicy=0
DefaultFormat=2
Path=/Users/Shared/WoW/Data/
SSCounter=101
displayItemAndNPCId=0



