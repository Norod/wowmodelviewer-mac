/*
 * GameFile.h
 *
 *  Created on: 27 oct. 2014
 *      Author: Jerome
 */

#ifndef _GAMEFILE_H_
#define _GAMEFILE_H_

#include <string>
#include <vector>

#if defined(_WINDOWS)
#include <windows.h>
typedef unsigned char uint8;
typedef char int8;
typedef unsigned __int16 uint16;
typedef __int16 int16;
typedef unsigned __int32 uint32;
typedef __int32 int32;
typedef int ssize_t;
#else
#include <stdint.h>
typedef uint8_t uint8;
typedef int8_t int8;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint32_t uint32;
typedef int32_t int32;
#endif

#include "metaclasses/Component.h"

#ifdef _WIN32
#    ifdef BUILDING_CORE_DLL
#        define _GAMEFILE_API_ __declspec(dllexport)
#    else
#        define _GAMEFILE_API_ __declspec(dllimport)
#    endif
#else
#    define _GAMEFILE_API_
#endif

class _GAMEFILE_API_ GameFile : public Component
{
  public:
    GameFile(QString path, int id = -1) 
      : eof(true), buffer(nullptr), pointer(0), size(0), 
        filepath(path), m_useMemoryBuffer(true), m_fileDataId(id),
        originalBuffer(nullptr), curChunk("")
    {}

    virtual ~GameFile() {}

    virtual size_t read(void* dest, size_t bytes);
    size_t getSize();
    size_t getPos();
    unsigned char* getBuffer() const;
    unsigned char* getPointer();
    bool isEof();
    virtual void seek(size_t offset);
    void seekRelative(size_t offset);
    bool open(bool useMemoryBuffer = true);
    bool close();
    
    void setFullName(const QString & name) { filepath = name; }
    QString fullname() const { return filepath; }
    int fileDataId() { return m_fileDataId; }

    void allocate(unsigned long long size);
    bool setChunk(std::string chunkName, bool resetToStart = true);
    bool isChunked() { return chunks.size() > 0; }

    virtual void dumpStructure();

  protected:

    virtual bool openFile() = 0;
    virtual bool isAlreadyOpened() = 0;
    virtual bool getFileSize(unsigned long long & s) = 0;
    virtual unsigned long readFile() = 0;
    virtual void doPostOpenOperation() = 0;
    virtual bool doPostCloseOperation() = 0;

    bool eof;
    unsigned char *buffer;
    unsigned long long pointer, size;
    QString filepath;
    int m_fileDataId;
    
    struct chunkHeader
    {
      char magic[4];
      uint32 size;
    };

    struct Chunk
    {
      std::string magic;
      unsigned int start;
      unsigned int size;
      unsigned int pointer;
    };

    std::vector<Chunk> chunks;
    bool m_useMemoryBuffer;

  private:
    // disable copying
    GameFile(const GameFile &);
    void operator=(const GameFile &);
    unsigned char * originalBuffer;
    std::string curChunk;
};



#endif /* _GAMEFILE_H_ */
